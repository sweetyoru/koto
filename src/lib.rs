#![allow(non_snake_case)]			// turn off warning about snake_case name
#![allow(non_camel_case_types)]		// turn off warning about upper camel case type name
#![allow(non_upper_case_globals)]	// turn off warning about global non upper camel name
#![feature(maybe_uninit_uninit_array)]
#![feature(maybe_uninit_array_assume_init)]


use std::hash::Hash;
use std::mem::{self, MaybeUninit};
use std::collections::{BTreeSet, BTreeMap, HashMap};


pub trait KotoTrait: Sized
{
	fn OutputBytesSize (&self) -> usize;
	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>;
	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>;

	fn ToBytes (&self) -> Result<Vec<u8>, KotoError>
	{
		let mut result = vec! [0u8; self.OutputBytesSize ()];
		self.ToBytesInplace (&mut result)?;
		Ok(result)
	}
}


impl KotoTrait for ()
{
	fn OutputBytesSize (&self) -> usize { 0 }

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		Ok(((), 0))
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		Ok(0)
	}
}


impl KotoTrait for bool
{
	fn OutputBytesSize (&self) -> usize { 1 }

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		match data.get (0).ok_or_else (|| KotoError::NotEnoughData(1))?
		{
			0 => Ok((false, 1)),
			1 => Ok((true, 1)),
			_ => Err(KotoError::InvalidData(data[0..1].to_vec ())),
		}
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		let outputBytesSize = self.OutputBytesSize ();

		if output.len () < outputBytesSize
		{
			return Err(KotoError::NotEnoughSpace(outputBytesSize));
		}

		match self
		{
			true => output[0] = 1,
			false => output[1] = 0,
		}

		Ok(outputBytesSize)
	}
}


macro_rules! AutoNumberImplement {
	($a:ty) =>
	{
		impl KotoTrait for $a
		{
			fn OutputBytesSize (&self) -> usize { mem::size_of::<Self> () }

			fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
			{
				if data.len () < mem::size_of::<Self> ()
				{
					return Err(KotoError::NotEnoughData(mem::size_of::<Self> ()));
				}

				Ok((Self::from_be_bytes (data.try_into ().unwrap ()), mem::size_of::<Self> ()))
			}

			fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
			{
				let outputBytesSize = self.OutputBytesSize ();

				if output.len () < outputBytesSize
				{
					return Err(KotoError::NotEnoughSpace(outputBytesSize));
				}

				output.copy_from_slice (&self.to_be_bytes ());

				Ok(outputBytesSize)
			}
		}
	};
}


AutoNumberImplement! (u8);
AutoNumberImplement! (u16);
AutoNumberImplement! (u32);
AutoNumberImplement! (u64);
AutoNumberImplement! (u128);


impl<T: KotoTrait, const N: usize> KotoTrait for [T; N]
{
	fn OutputBytesSize (&self) -> usize
	{
		// size + element count + size per element * element count
		let mut size = mem::size_of::<u64> () + mem::size_of::<u64> ();

		for item in self
		{
			size += item.OutputBytesSize ();
		}

		size
	}

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		// check size
		if data.len () < 16
		{
			return Err(KotoError::NotEnoughData(16));
		}

		let (dataSize, _) = u64::FromBytes (&data[0..8])?;
		let dataSize = dataSize.try_into ().unwrap ();

		let (elementCount, _) = u64::FromBytes (&data[8..16])?;
		let elementCount = elementCount.try_into ().unwrap ();


		// check element count
		if elementCount != N
		{
			return Err(KotoError::NotEnoughData(mem::size_of::<T> ()*N));
		}


		// check data size
		if data.len () < dataSize
		{
			return Err(KotoError::NotEnoughData(dataSize));
		}


		// get data
		let mut result = MaybeUninit::<T>::uninit_array ();
		let mut currentIndex = 16;

		for index in 0..elementCount
		{
			let (nextElement, nextElementSize) = T::FromBytes (&data[currentIndex..])?;
			result[index].write (nextElement);
			currentIndex += nextElementSize;
		}


		// check size
		if currentIndex != dataSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, dataSize));
		}


		unsafe { Ok((MaybeUninit::array_assume_init (result), dataSize)) }
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		// check output buffer size
		let outputBytesSize = self.OutputBytesSize ();
		let elementCount: u64 = self.len ().try_into ().unwrap ();

		if output.len () < outputBytesSize
		{
			return Err(KotoError::NotEnoughSpace(outputBytesSize));
		}


		// write size
		output[0..8].copy_from_slice (&outputBytesSize.to_be_bytes ());
		output[8..16].copy_from_slice (&elementCount.to_be_bytes ());


		// write each element
		let mut currentIndex = 16;

		for element in self
		{
			currentIndex += element.ToBytesInplace (&mut output[currentIndex..])?;
		}


		// check size
		if currentIndex != outputBytesSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, outputBytesSize));
		}


		Ok(outputBytesSize)
	}
}


impl<T: KotoTrait> KotoTrait for &[T]
{
	fn OutputBytesSize (&self) -> usize
	{
		// size + element count + size per element * element count
		let mut size = mem::size_of::<u64> () + mem::size_of::<u64> ();

		for item in *self
		{
			size += item.OutputBytesSize ();
		}

		size
	}

	fn FromBytes (_data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		Err(KotoError::UnsupportedType)
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		// check output buffer size
		let outputBytesSize = self.OutputBytesSize ();
		let elementCount: u64 = self.len ().try_into ().unwrap ();

		if output.len () < outputBytesSize
		{
			return Err(KotoError::NotEnoughSpace(outputBytesSize));
		}


		// write size
		output[0..8].copy_from_slice (&outputBytesSize.to_be_bytes ());
		output[8..16].copy_from_slice (&elementCount.to_be_bytes ());


		// write each element
		let mut currentIndex = 16;

		for element in *self
		{
			currentIndex += element.ToBytesInplace (&mut output[currentIndex..])?;
		}


		// check size
		if currentIndex != outputBytesSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, outputBytesSize));
		}


		Ok(outputBytesSize)
	}
}


impl<T: KotoTrait> KotoTrait for Vec<T>
{
	fn OutputBytesSize (&self) -> usize
	{
		// size + element count + size per element * element count
		let mut size = mem::size_of::<u64> () + mem::size_of::<u64> ();

		for item in self
		{
			size += item.OutputBytesSize ();
		}

		size
	}

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		//check size
		if data.len () < 16
		{
			return Err(KotoError::NotEnoughData(16));
		}

		let (dataSize, _) = u64::FromBytes (&data[0..8])?;
		let dataSize = dataSize.try_into ().unwrap ();

		let (elementCount, _) = u64::FromBytes (&data[8..16])?;
		let elementCount = elementCount.try_into ().unwrap ();


		// check data size
		if data.len () < dataSize
		{
			return Err(KotoError::NotEnoughData(dataSize));
		}


		// get data
		let mut result = Vec::new ();
		let mut currentIndex = 16;

		for _ in 0..elementCount
		{
			let (nextElement, nextElementSize) = T::FromBytes (&data[currentIndex..])?;
			result.push (nextElement);
			currentIndex += nextElementSize;
		}


		// check size
		if currentIndex != dataSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, dataSize));
		}


		Ok((result, dataSize))
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		// check output buffer size
		let outputBytesSize = self.OutputBytesSize ();
		let elementCount: u64 = self.len ().try_into ().unwrap ();

		if output.len () < outputBytesSize
		{
			return Err(KotoError::NotEnoughSpace(outputBytesSize));
		}


		// write size
		output[0..8].copy_from_slice (&outputBytesSize.to_be_bytes ());
		output[8..16].copy_from_slice (&elementCount.to_be_bytes ());


		// write each element
		let mut currentIndex = 16;

		for element in self
		{
			currentIndex += element.ToBytesInplace (&mut output[currentIndex..])?;
		}


		// check size
		if currentIndex != outputBytesSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, outputBytesSize));
		}


		Ok(outputBytesSize)
	}
}


impl KotoTrait for &str
{
	fn OutputBytesSize (&self) -> usize
	{
		self.as_bytes ().OutputBytesSize ()
	}

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		Err(KotoError::UnsupportedType)
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		self.as_bytes ().ToBytesInplace (output)
	}
}


impl KotoTrait for String
{
	fn OutputBytesSize (&self) -> usize
	{
		self.as_bytes ().OutputBytesSize ()
	}

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		let (inner, size) = Vec::<u8>::FromBytes (data)?;
		Ok((Self::from_utf8 (inner)?, size))
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		self.as_bytes ().ToBytesInplace (output)
	}
}


impl<T: KotoTrait + Ord> KotoTrait for BTreeSet<T>
{
	fn OutputBytesSize (&self) -> usize
	{
		// size + element count + size per element * element count
		let mut size = mem::size_of::<u64> () + mem::size_of::<u64> ();

		for item in self
		{
			size += item.OutputBytesSize ();
		}

		size
	}

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		//check size
		if data.len () < 16
		{
			return Err(KotoError::NotEnoughData(16));
		}

		let (dataSize, _) = u64::FromBytes (&data[0..8])?;
		let dataSize = dataSize.try_into ().unwrap ();

		let (elementCount, _) = u64::FromBytes (&data[8..16])?;
		let elementCount = elementCount.try_into ().unwrap ();


		// check data size
		if data.len () < dataSize
		{
			return Err(KotoError::NotEnoughData(dataSize));
		}


		// get data
		let mut result = BTreeSet::new ();
		let mut currentIndex = 16;

		for _ in 0..elementCount
		{
			let (nextElement, nextElementSize) = T::FromBytes (&data[currentIndex..])?;
			result.insert (nextElement);
			currentIndex += nextElementSize;
		}


		// check size
		if currentIndex != dataSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, dataSize));
		}


		Ok((result, dataSize))
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		// check output buffer size
		let outputBytesSize = self.OutputBytesSize ();
		let elementCount: u64 = self.len ().try_into ().unwrap ();

		if output.len () < outputBytesSize
		{
			return Err(KotoError::NotEnoughSpace(outputBytesSize));
		}


		// write size
		output[0..8].copy_from_slice (&outputBytesSize.to_be_bytes ());
		output[8..16].copy_from_slice (&elementCount.to_be_bytes ());


		// write each element
		let mut currentIndex = 16;

		for element in self
		{
			currentIndex += element.ToBytesInplace (&mut output[currentIndex..])?;
		}


		// check size
		if currentIndex != outputBytesSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, outputBytesSize));
		}


		Ok(outputBytesSize)
	}
}


impl<K: KotoTrait + Ord, V: KotoTrait> KotoTrait for BTreeMap<K, V>
{
	fn OutputBytesSize (&self) -> usize
	{
		// size + element count + size per element * element count
		let mut size = mem::size_of::<u64> () + mem::size_of::<u64> ();

		for (key, value) in self
		{
			size += key.OutputBytesSize () + value.OutputBytesSize ();
		}

		size
	}

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		//check size
		if data.len () < 16
		{
			return Err(KotoError::NotEnoughData(16));
		}

		let (dataSize, _) = u64::FromBytes (&data[0..8])?;
		let dataSize = dataSize.try_into ().unwrap ();

		let (elementCount, _) = u64::FromBytes (&data[8..16])?;
		let elementCount = elementCount.try_into ().unwrap ();


		// check data size
		if data.len () < dataSize
		{
			return Err(KotoError::NotEnoughData(dataSize));
		}


		// get data
		let mut result = BTreeMap::new ();
		let mut currentIndex = 16;

		for _ in 0..elementCount
		{
			let (nextElementKey, nextElementSize) = K::FromBytes (&data[currentIndex..])?;
			currentIndex += nextElementSize;

			let (nextElementValue, nextElementSize) = V::FromBytes (&data[currentIndex..])?;
			currentIndex += nextElementSize;

			result.insert (nextElementKey, nextElementValue);
		}


		// check size
		if currentIndex != dataSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, dataSize));
		}


		Ok((result, dataSize))
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		// check output buffer size
		let outputBytesSize = self.OutputBytesSize ();
		let elementCount: u64 = self.len ().try_into ().unwrap ();

		if output.len () < outputBytesSize
		{
			return Err(KotoError::NotEnoughSpace(outputBytesSize));
		}


		// write size
		output[0..8].copy_from_slice (&outputBytesSize.to_be_bytes ());
		output[8..16].copy_from_slice (&elementCount.to_be_bytes ());


		// write each element
		let mut currentIndex = 16;

		for (key, value) in self
		{
			currentIndex += key.ToBytesInplace (&mut output[currentIndex..])?;
			currentIndex += value.ToBytesInplace (&mut output[currentIndex..])?;
		}


		// check size
		if currentIndex != outputBytesSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, outputBytesSize));
		}


		Ok(outputBytesSize)
	}
}


impl<K: KotoTrait + Eq + Hash, V: KotoTrait> KotoTrait for HashMap<K, V>
{
	fn OutputBytesSize (&self) -> usize
	{
		// size + element count + size per element * element count
		let mut size = mem::size_of::<u64> () + mem::size_of::<u64> ();

		for (key, value) in self
		{
			size += key.OutputBytesSize () + value.OutputBytesSize ();
		}

		size
	}

	fn FromBytes (data: &[u8]) -> Result<(Self, usize), KotoError>
	{
		//check size
		if data.len () < 16
		{
			return Err(KotoError::NotEnoughData(16));
		}

		let (dataSize, _) = u64::FromBytes (&data[0..8])?;
		let dataSize = dataSize.try_into ().unwrap ();

		let (elementCount, _) = u64::FromBytes (&data[8..16])?;
		let elementCount = elementCount.try_into ().unwrap ();


		// check data size
		if data.len () < dataSize
		{
			return Err(KotoError::NotEnoughData(dataSize));
		}


		// get data
		let mut result = HashMap::new ();
		let mut currentIndex = 16;

		for _ in 0..elementCount
		{
			let (nextElementKey, nextElementSize) = K::FromBytes (&data[currentIndex..])?;
			currentIndex += nextElementSize;

			let (nextElementValue, nextElementSize) = V::FromBytes (&data[currentIndex..])?;
			currentIndex += nextElementSize;

			result.insert (nextElementKey, nextElementValue);
		}


		// check size
		if currentIndex != dataSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, dataSize));
		}


		Ok((result, dataSize))
	}

	fn ToBytesInplace (&self, output: &mut [u8]) -> Result<usize, KotoError>
	{
		// check output buffer size
		let outputBytesSize = self.OutputBytesSize ();
		let elementCount: u64 = self.len ().try_into ().unwrap ();

		if output.len () < outputBytesSize
		{
			return Err(KotoError::NotEnoughSpace(outputBytesSize));
		}


		// write size
		output[0..8].copy_from_slice (&outputBytesSize.to_be_bytes ());
		output[8..16].copy_from_slice (&elementCount.to_be_bytes ());


		// write each element
		let mut currentIndex = 16;

		for (key, value) in self
		{
			currentIndex += key.ToBytesInplace (&mut output[currentIndex..])?;
			currentIndex += value.ToBytesInplace (&mut output[currentIndex..])?;
		}


		// check size
		if currentIndex != outputBytesSize
		{
			return Err(KotoError::DataSizeMisMatch(currentIndex, outputBytesSize));
		}


		Ok(outputBytesSize)
	}
}


#[derive(Debug)]
pub enum KotoError
{
	// expected data size
	NotEnoughData(usize),
	// expected output buffer size
	NotEnoughSpace(usize),
	// the data attempt to parse
	InvalidData(Vec<u8>),

	DataSizeMisMatch(usize, usize),

	Utf8Error(std::str::Utf8Error),

	UnsupportedType,
}


impl From<std::str::Utf8Error> for KotoError
{
	fn from (error: std::str::Utf8Error) -> Self
	{
		Self::Utf8Error(error)
	}
}


impl From<std::string::FromUtf8Error> for KotoError
{
	fn from (error: std::string::FromUtf8Error) -> Self
	{
		Self::Utf8Error(error.utf8_error ())
	}
}
